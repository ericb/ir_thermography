This folder contains the sources of the software who will allow to make thermal images analize, and improve them.

It's based on [**miniDart**](https://framagit.org/ericb/miniDart)  (performance analysis software). All useless features coming from miniDart will be progressively removed.

The current version works on Linux only  (I'll propose a Windows version very soon, I just miss some time to package it.

Ce dossier contient les sources du logiciel qui permettra de faire l'analyse des images thermiques, ainsi que leur traitement.

Il est basé sur [**miniDart**](https://framagit.org/ericb/miniDart) (logiciel d'analyse de performance Handball) et sera progressivement simplifié.

Cette version ne fonctionne que sous Linux (une version -payante- pour Windows, sera toutefois proposée), les sources étant quasiment les mêmes, excepté la partie en interaction avec le système d'exploitation (ouverture d'un fichier, sauvegarde, recheche sur le disque, etc).


## [French] Comment utiliser SeekGui ?

1. Compilation 

### Ports fonctionnels :

- [X] **Linux Intel (x86_64)**

- [X] **Linux ARM v8** (aarch64). Avec OpenGL 3.0 ou OpenGL ES 3.1. Tests en cours avec un RPi 4B/8GB (ça devrait fonctionner aussi avec 4GB).

### À venir :

- [ ] **Linux ARM 7 (armhf)** : non testé (très bientôt, depuis que le port sous OpenGLES 3.1 fonctionne)

- [ ] **Windows 7+** : WIP  (par cross-compilation avec mingw + libusb + OpenCV + OpenGL + ImGui)


2. Détection de la caméra : 

2.1 Installer usbutils 

sudo apt-get install usbutils

2.2 Test

- brancher la caméra (port USB)

- Dans un terminal essayer la commande : lsusb --list-devices 

2.2.1  Si la caméra est détectée

Essayer de visualiser les images avec cheese ou qv4l2 (à installer aussi)

2.2.2 Si elle n'est pas détectée :

Installer la bibliothèque libseek-thermal, qu'il faudra compiler soit même. [**Lien pour télécharger les sources**](). Les dépendances sont OpenCV et libusb-1.0-0-dev

Une fois la compilation terminée, il faudra utiliser le binaire seek-viewer placé dans examples, dans le répertoire où tout a été compilé.

Installer v4l2loopback : attention, il faut compiler le module, et pour cela, il faudra installer les sources de votre noyau Linux

Les sources de v4l2loopback sont téléchargeable via git, sinon une archive (.tar.gz ou .zip) => [**Le dépôt est là**](https://github.com/umlaeute/v4l2loopback)

Ensuite suivre la méthode habituelle : make && sudo make install  # en supposant que vous avez satisfait toutes les dépendances

Pour charger le module v4l2loopback, taper les lignes suivantes dans le terminal

````
sudo depmod -a 

sudo modprobe v4l2loopback devices=3
````

Pour afficher les images, il faut que la caméra soit initialisée par seek_viewer, qui enverra les images vers le périphérique virtuel /dev/video0

Ensuite, SeekGui ira lire ces images et pourra les afficher, les modifier, les enregistrer aussi (pas encore fait).

La commmande à taper avec seek_viewer est : 

````
./seek_viewer --camtype=seekpro --colormap=11 --mode=v4l2 --output=/dev/video0 --scale=2
````

N.B. : le paramètre --scale=2 est optionnel.  seek_viewer -h  pour en savoir plus sur les options disponibles.

TODO : intégrer seek_viewver dans SeekGui

En cas de problème, ouvrir une issue. Sinon, si tout s'est bien passé, brancher la caméra, et lancer SeekGui.

**La caméra thermique sera normalement vue comme la caméra numéro 1**, avec une fréquence d'affichage de ~ 16 Hz

Important : la caméra testée est le modéle 0x289d / 0x0011 (Seek Thermal compact pro)

La loupe fonctionne déjà, ainsi que les annotations, et quelques réglages.


TODO : 

- [WIP] permettre d'enregistrer les images dans une vidéo. Actuellement fonctionnel (testé sur RPi 4 / 8Go : 960 x 540 (ok))

- [WIP] inclure l'échelle de température et la bibliothèque SeekThermal, ainsi que thermal_viewer






## Fonctionnalités présentes 

Fonctionnalité attendues (déjà présentes, d'autres inutiles ici seront progressivement supprimées) :

- [x] **utilisable sous Linux** (Mesa + OpenGL) et sous Windows (OpenGL). Il est impératif d'installer les pilotes OpenGL

- [x] Utilise OpenCV en natif : les images reçus de la caméra sont transformées en objets de type cv::Mat, qui permet l'utilisation directe d'OpenCV

- [x] visualiser les images venant d'une camera thermique IR. 720p@fps. fps étant à définir, car dépend de la caméra : tests prévus // FIXME

- [x] pouvoir isoler une partie de l'image et zoomer  // Loupe, fonctionnelle  

- [x] pouvoir enregistrer les images  (.avi ou .mp4)

- [x] pouvoir annoter une image  // Canvas, déjà fonctionnel

- [x] pouvoir dessiner sur une image  // Canvas, déjà fonctionnel

- [x] pouvoir enregistrer l'image annotée // à terminer


## Fonctionnalités restant à implémenter

- intégration de la calibration de la caméra ? (en runtime ?)

- filtrage d'images (algorithmes d'interpolation, smoothing, amélioration signal/(signal+bruit)

- tracé d'histogrammes en temps réel avec [**OpenCV**](https://github.com/opencv/opencv) et [**ImPlot**](https://github.com/epezent/implot)

- enregistrement coordonné avec creation d'un fichier associé type .xml

- connexion à une base de donnée (sqlite)




