/*
 * engine.cpp file, from miniDart project
 * Author : Eric Bachard  / lundi 3 octobre 2016, 14:35:03 (UTC+0200)
 * This file is under GPL v2 License
 * See : http://www.gnu.org/licenses/gpl-2.0.html
 */

#include "engine.hpp"                  // class Engine
#include "application.hpp"             // WINDOW_WIDTH, WINDOW_HEIGHT,
#include "sdl_utils.hpp"               // getInfo()

#include "SDL2/SDL.h"                  // We use SDL2

#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"
#include <GL/gl.h>                     // OpenGL

#ifdef BUILDING_FRENCH
#include "miniDart_fr.hpp"
#else
#include "miniDart_en-US.hpp"
#endif

using std::cout;
using std::endl;

#ifdef NATIVE_BUILD
// Linux working value

#ifdef AARCH64_OPENGL30_MAX
static const float kSysDefaultDpi = 96.0f;
#else
static const float kSysDefaultDpi = 150.0f;
#endif

#else
// Windows value. Needs more love ...
static const float kSysDefaultDpi = 96.0f;
#endif

// taken from https://nlguillemot.wordpress.com/2016/12/11/high-dpi-rendering/
//#if defined(_WIN32)
void Engine::getSystemDisplayDPI(int displayIndex, float * dpi, float * defaultDpi)
{
    if (SDL_GetDisplayDPI(displayIndex, NULL, dpi, NULL) != 0)
    {
        if (dpi)
           *dpi = kSysDefaultDpi; // Failed to get DPI, so just return the default value.
    }
    // FIXME Looks buggy, bizarre :-/
    if (defaultDpi)
        *defaultDpi = kSysDefaultDpi;
}
//#endif

Engine::Engine()
{
    int anErr = init_SDL();

    if (anErr != 0)
    {
        std::cerr << "Cannot initialize SDL or OpenGL. Exiting" << std::endl;
        clean_and_close();
    }
}


Engine::~Engine()
{
    clean_and_close();
}


int Engine::init_SDL()
{
    // https://www.gog.com/forum/thimbleweed_park/linux_unable_to_init_sdl_mixer_alsa_couldnt_open_audio_device_no_such_device
    // https://wiki.libsdl.org/FAQUsingSDL
#ifndef NATIVE_BUILD
    SDL_setenv("SDL_AUDIODRIVER", "DirectSound", true);
    putenv((char *)"SDL_AUDIODRIVER=DirectSound");
#else
    SDL_setenv("SDL_AUDIODRIVER", "alsa", true);
    putenv((char *)"SDL_AUDIODRIVER=alsa");
    //putenv((char *)"SDL_AUDIODEV=pulse");
#endif

#ifdef AARCH64_OPENGL30_MAX
    putenv((char *)"MESA_GL_VERSION_OVERRIDE=3.0");
    putenv((char *)"MESA_GLSL_VERSION_OVERRIDE=130");
#endif
    // about issues between OpenGL 3.3. and OpenGL 3.0 (only Linux concerned)
    // https://discourse.libsdl.org/t/confused-about-what-opengl-context-is-being-used-with-sdl/22860
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        std::cerr << "Failed to initialize SDL: " << SDL_GetError() << std::endl;
        return 1;
    }

    // removed for TESTS, and no apparent change at least on Linux ...
    // SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
#ifdef AARCH64_OPENGL30_MAX
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#else
    // 3.2 core profile should be sufficient (true on Windows)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
#endif
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

    SDL_GetCurrentDisplayMode(0, &current);

    SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );

    // 1 == enable VSync ; 0 == disable VSync
    #define USE_VSYNC
    #ifdef USE_VSYNC
    SDL_GL_SetSwapInterval(1);
    #else
    SDL_GL_SetSwapInterval(0);
    #endif

    window = nullptr;

// HIGH DPI FOR ALL !

    mWidth = WINDOW_WIDTH;
    mHeight = WINDOW_HEIGHT;

    windowDpiScaledWidth    = WINDOW_WIDTH;
    windowDpiScaledHeight   = WINDOW_HEIGHT;
    windowDpiUnscaledWidth  = WINDOW_WIDTH;
    windowDpiUnscaledHeight = WINDOW_HEIGHT;

    dpi          = kSysDefaultDpi;
    defaultDpi   = kSysDefaultDpi;
    displayIndex = 0;

    getSystemDisplayDPI(displayIndex, &dpi, &defaultDpi);

    windowDpiScaledWidth = int(windowDpiUnscaledWidth * dpi / defaultDpi);
    windowDpiScaledHeight = int(windowDpiUnscaledHeight * dpi / defaultDpi);

    std::cerr << "dpi                   : " << dpi << "\n";
    std::cerr << "defaultDpi            : " << defaultDpi << "\n";

    std::cerr << "windowDpiScaledWidth  : " << windowDpiScaledWidth << "\n";
    std::cerr << "windowDpiScaledHeight : " << windowDpiScaledHeight << "\n";

    window = SDL_CreateWindow(MINIDART_VERSION_NUMBER,
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              windowDpiScaledWidth,
                              windowDpiScaledHeight,
                              SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE|SDL_WINDOW_ALLOW_HIGHDPI);

    // check whether the SDL2 window exists
    if (getWindow() == nullptr)
        sdl_application_abort("Problem creating the SDL window.\n");
    else
        std::cout << "SDL2 Window created " << "\n";

    SDL_GetWindowSize(getWindow(), &mWidth, &mHeight);

    std::cout <<  "SDL_VERSION_ATLEAST(2,0,9) " << SDL_VERSION_ATLEAST(2,0,7) <<  std::endl;
    std::cout << "SDL_HAS_CAPTURE_AND_GLOBAL_MOUSE    SDL_VERSION_ATLEAST(2,0,4) = " << SDL_VERSION_ATLEAST(2,0,4) << std::endl;
    std::cout << "SDL_HAS_WINDOW_ALPHA                SDL_VERSION_ATLEAST(2,0,5) = " << SDL_VERSION_ATLEAST(2,0,5) << std::endl;
    std::cout << "SDL_HAS_ALWAYS_ON_TOP               SDL_VERSION_ATLEAST(2,0,5) = " << SDL_VERSION_ATLEAST(2,0,5) << std::endl;
    std::cout << "SDL_HAS_USABLE_DISPLAY_BOUNDS       SDL_VERSION_ATLEAST(2,0,5) = " << SDL_VERSION_ATLEAST(2,0,5) << std::endl;
    std::cout << "SDL_HAS_PER_MONITOR_DPI             SDL_VERSION_ATLEAST(2,0,4) = " << SDL_VERSION_ATLEAST(2,0,4) << std::endl;
    std::cout << "SDL_HAS_VULKAN                      SDL_VERSION_ATLEAST(2,0,6) = " << SDL_VERSION_ATLEAST(2,0,6) << std::endl;
    std::cout << "SDL_HAS_MOUSE_FOCUS_CLICKTHROUGH    SDL_VERSION_ATLEAST(2,0,5) = " << SDL_VERSION_ATLEAST(2,0,5) << std::endl;

    //    seems to work very well. Just an issue : how to handle the window and move it ? (ALT + is suboptimal)
    //    SDL_SetWindowBordered(window, SDL_FALSE);

    const SDL_GLContext & glcontext = SDL_GL_CreateContext(getWindow());

    if (glcontext == nullptr)
        sdl_application_abort("Problem creating GL context.\n");

#ifdef AARCH64_OPENGL30_MAX
    #ifndef GL_EXT_PROTOTYPES
        #define GL_EXT_PROTOTYPES 1
    #endif

    #ifndef GL3_PROTOTYPES
        #define GL3_PROTOTYPES 1
    #endif

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
#else
    if (gl3wInit() != 0)
#endif
    {
        SDL_GL_DeleteContext(glcontext);
        SDL_Quit();
        throw std::runtime_error("Failed to initialize OpenGL\n");
    }
    getInfo();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_BLEND);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);

    int drawable_width, drawable_height;
    SDL_GL_GetDrawableSize(getWindow(), &drawable_width, &drawable_height);

    // FIXME : implement AudioDevicesList class 
    int count = 8;

    for (int i = 0; i < count; ++i) {
        fprintf(stdout, "Audio device (no recording capability)   %d: %s\n", i, SDL_GetAudioDeviceName(i, 0));
        fprintf(stdout, "Audio device (with recording capability) %d: %s\n", i, SDL_GetAudioDeviceName(i, 1));
    }

    const char* driver_name = SDL_GetCurrentAudioDriver();

    if (driver_name) {
        printf("Audio subsystem initialized; driver = %s.\n", driver_name);
    } else {
        printf("Audio subsystem not initialized.\n");
    }

    return EXIT_SUCCESS;
}

void Engine::clean_and_close()
{
    SDL_DestroyWindow(getWindow());
    window = nullptr;
    SDL_GL_DeleteContext(glcontext);
    SDL_Quit();
}





