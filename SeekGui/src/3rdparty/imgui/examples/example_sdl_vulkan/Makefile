# Linux (all archs) generic Makefile
# Copyright Eric Bachard / 2019 March 29th
# License MIT



UNAME_S = $(shell uname -s)

ifeq (${UNAME_S}, Linux)

# on Linux, returns the arch name
UNAME_M = $(shell uname -m)

UNAME_P = $(shell uname -p)
# use GPU_TYPE = Intel in case of inxi is not available, or is too slow
ifeq (${UNAME_P}, aarch64)
GPU_TYPE = armv8
endif

ifeq (${UNAME_P}, x86_64)
#GPU_TYPE = $(shell inxi -G | cut -d" " -f4 -s -z 2>/dev/null)_
# faster method
GPU_TYPE = intel
endif

# change or comment me if you want to select another g++ version
#VERSION=-7

CXX = g++${VERSION}
CXX_STANDARD = -std=c++11




INCLUDE_DIR = -I./../.. -I..

# often src, or something close
SOURCES_DIR = .
BUILD_DIR = .

APPLICATION_NAME = imgui_vulkan_${GPU_TYPE}-${UNAME_M}
FILENAME = ${BUILD_DIR}/${APPLICATION_NAME}

CXX_FLAGS = -DIMGUI_UNLIMITED_FRAME_RATE -D_NO_DEBUG_HEAP=1 -Wall ${CXX_STANDARD}

# Some dependencies :
# SDL2 + libvulkan-dev and libvulkan1 must be installed (or Vulkan SDK 1.1.x must be installed and detected)
# + a recent version of mesa, built from git) mesa 19.1 for libvulkan_intel is needed
SDL2_FLAGS = `sdl2-config --cflags --libs`

# Change for the right libs if you are using NVidia version
VULKAN_LDFLAGS = -L/usr/lib/${UNAME_M}-linux-gnu -lvulkan

LDFLAGS = -lGL -lGLU  ${SDL2_FLAGS} ${VULKAN_LDFLAGS}

DEBUG_SUFFIX = _debug
CXX_FLAGS_DEBUG = -g -DDEBUG -DIMGUI_VULKAN_DEBUG_REPORT


FILES = ${SOURCES_DIR}/main.cpp \
        ${SOURCES_DIR}/../../imgui.cpp \
        ${SOURCES_DIR}/../../imgui_demo.cpp  \
        ${SOURCES_DIR}/../../imgui_draw.cpp \
        ${SOURCES_DIR}/../../imgui_widgets.cpp \
        ${SOURCES_DIR}/../imgui_impl_vulkan.cpp \
        ${SOURCES_DIR}/../imgui_impl_sdl.cpp

all : clean ${FILENAME} ${FILENAME}${DEBUG_SUFFIX}

${FILENAME}:
	${CXX} ${INCLUDE_DIR} ${CXX_FLAGS} -o $@ ${FILES} ${LDFLAGS}

${FILENAME}${DEBUG_SUFFIX}: ${OBJS}
	${CXX} ${INCLUDE_DIR} ${CXX_FLAGS} ${CXX_FLAGS_DEBUG} -o $@ ${FILES} ${LDFLAGS}

clean:
	${RM} *.o ${FILENAME} ${FILENAME}${DEBUG_SUFFIX}

endif
