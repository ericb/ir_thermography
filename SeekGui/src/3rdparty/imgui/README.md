Eric Bachard   2020/04/13


Last ImGui version used in miniDart is : 

+ origin/docking branch (in sync with Dear ImGui 1.76) rev. 01301373  (2020/04/13)

+ + all the changes mentionned in the Dear_ImGui_changes_in_miniDart_2020_04_13_ericb.diff file

For the record :
````
git pull
remote: Enumerating objects: 4, done.
remote: Counting objects: 100% (4/4), done.
remote: Total 4 (delta 3), reused 4 (delta 3), pack-reused 0
Dépaquetage des objets: 100% (4/4), fait.
Depuis https://github.com/ocornut/imgui
   b8e2b2bd..01301373  docking    -> origin/docking
Mise à jour b8e2b2bd..01301373
Fast-forward
 examples/imgui_impl_dx12.cpp | 17 +++++++++++++++--
 1 file changed, 15 insertions(+), 2 deletions(-)
````

