/*
 * scoreboard.hpp file, from miniDart project
 * Author : Eric Bachard  / monday 2020 9th August, 14:35:03 (UTC+0200)
 * This file is under GPL v2 License
 * See : http://www.gnu.org/licenses/gpl-2.0.html
 */

#ifndef SCOREBOARD_HPP
#define SCOREBOARD_HPP

#include "imgui.h"

struct sbColors
{
    Color backgroundColor;
    Color borderColor;
    Color fontColor;
} SbColors;


namespace md

{
    class ScoreBoard
    {
        public:
            // Ctor
            ScoreBoard();

            // Dtor
           ~ScoreBoard();

            ImVec2 dimensions;
            ImVec2 position;
            sbControler;
    }
}


#endif /* SCOREBOARD_HPP */