## French

1. Le site de la bibliothèque qui prend en charge la caméra seek (toutes les caméras USB C et micro USB,
ainsi que les caméras avec connecteur ligthning :

  - [**dépôt github de la bibliothèque libseek**](https://github.com/OpenThermal/libseek-thermal/)

  - Correction de warnings à la compilation + [**la requête d'intégration associée**](https://github.com/ebachard/libseek-thermal/commit/f5c9e8093766b7f4074aba53c0f332ca133dfc0a)

2. Conversion de température K -> °C  (normalement connu)

  - [**quelques rappels**](https://github.com/lod/seek-thermal-documentation)

  - [**Théorie de l'imagerie thermique**](https://github.com/lod/seek-thermal-documentation/wiki/Thermal-Imaging-Theory)

3. Contrôle non destructif (liens à venir)

4. Calibration d'une webcam à l'aide d'OpenCV

  - [(en) **OpenCV documentation : camera calibration**](https://docs.opencv.org/3.4/d4/d94/tutorial_camera_calibration.html)

  - [(en) **Calibrating using OpenCV**](https://aishack.in/tutorials/calibrating-undistorting-opencv-oh-yeah/)


5. Echelle de couleurs pour le rendu (à venir)


  - [**Echelle de couleurs OpenCV**](https://docs.opencv.org/3.2.0/d3/d50/group__imgproc__colormap.html)

  FIXME

6. Amélioration de la qualité des images (OpenCV ?)

FIXME

TODO : recherche d'algorithmes (attention aux brevets)


7. Projet vert

- [**Analyse thermographique**](http://www.projetvert.fr/thermographie/analyse-thermographique/)

- [**Caméra thermique**](http://www.projetvert.fr/thermographie/camera-thermique/)

8. Définitions

- [**Thermographie**](https://bilans-thermiques.fr/thermographie)


## English





What ?

Source : https://www.eevblog.com/forum/thermal-imaging/thermal-camera-sdk-image-formats/msg3045864/#msg3045864

Something to be aware of when building your own radiometric analysis software. 
Some cameras apply histogram equalisation to the image to improve contrast and enhance the image.
Such ‘magic’ is often not detailed by the manufacturer and can totally destroy any radiometric
data that was present before the image enhancement.
I am working with a camera core that does exactly this and the SDK warns that the RGB image cannot be used
fir radiometric analysis as many pixel levels have been altered by the histogram equalisation process.
A separate radiometric data stream is provided by ghat core fir radiometric analysis.

From looking at several of the phone dongle type cameras, it would appear that there is a lot of image processing
and ‘tweaking’ of the image to decrease noise levels and make the images look pretty.
If an SDK does not offer access to RAW unmolested pixel data,
do not expect to use the camera with radiometric analysis software even if you extract the pixel values from the image.



Source : https://www.eevblog.com/forum/thermal-imaging/thermal-camera-sdk-image-formats/msg3045864/#msg3045864

    Country: cn

Re: Thermal Camera SDK, Image Formats
« Reply #6 on: May 04, 2020, 04:09:37 pm »
For Xtherm T3s and very likely for HT-301, it looks like we can get the raw sensor readout before any processing
after sending a command. This means we have the maximum freedom to do any math to get the image,
but we also have to implement everything by ourself including shutter calibration and dead pixel removal.

The output stream can be switched to other modes by commands, YUV data after applying color plates and 16bits numbers
with clear relation to radiometric values (can be converted to temperature mapping) are available.


2020 10 23

could contain interesting information. Keep it

Source : https://www.eevblog.com/forum/thermal-imaging/whats-going-on-with-seek-thermal-compact-pro/msg1775297/#msg1775297


2020 12 04 Ordered Ht-301 ht camera (waiting for delivery)

- SDK, usable with SeekGui



2020 12 06


**New, open extremely pro and very advanced project** :

Worth a read, and very probably more :

https://www.eevblog.com/forum/thermal-imaging/openirv-isc0901b0-(autoliv-nv3-flir-e4568)-based-opensource-thermal-camera/


New link : 

TODO : test it, looks promising

https://github.com/gtatters/Thermimage




