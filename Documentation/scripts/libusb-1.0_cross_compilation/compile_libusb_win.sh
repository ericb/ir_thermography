# this script alllows to cross compile libusb-1.0
#
# License MIT
# Eric Bachard  2020 / 09 / 20th

PREFIX=/usr/local/cross-tools/
TOOLSET=x86_64-w64-mingw32
CROSSPATH=$PREFIX/$TOOLSET
export CFLAGS="-I${CROSSPATH}/include"
export LDFLAGS="-L${CROSSPATH}/lib"

./configure --target=$TOOLSET --host=$TOOLSET \
             --build=x86_64-linux --prefix=$CROSSPATH --enable-static --disable-shared
 
# Make and install
make
#sudo make install
